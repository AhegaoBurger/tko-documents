# -*- coding: utf-8 -*-
{
    'name': "tko-documents",

    'summary': """
        Use this to print reports""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Nth Typonomy",
    'website': "https://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'reports/report.xml',
        'reports/report_template.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
