from odoo import fields, models


class ModuleReport(models.Model):
    _name = 'my.module'
    _description = 'Report Module'
    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=True)
    description = fields.Text()

    def print_report(self):
        return self.env.ref('my_module.my_module_report').report_action(self)